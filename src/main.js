import { createApp } from "vue";
import "bulma/css/bulma.min.css"
import router from "./router";
import App from "./App.vue";

const app = createApp(App);
app.use(router).mount("#app");
